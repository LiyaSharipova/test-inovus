# Описание
* Tomcat запускается во встроенном сервере из pom.xml c помощью ```mvn tomcat7:run```. Томкат  подключен к проекту с помощью ```tomcat7-maven-plugin```
* Логирование происходит в папке ```target/tomcat/logs/test-inovus.log```
* Логирование настраивается в ```META-INF/logback.xml```
* Контекст connection pool настраивается в ```META-INF/context.xml```. Подключается в плагине для томката
# Способ запуска
1. Перед первым запуском приложения нужно запустить создание БД ```flyway/init/init.sql``` . 
1. Запустить из командной строки```mvn clean package tomcat7:run``` . Будет запущен встроенный сервер Tomcat
1. Зайти на ```http://localhost:8080/```, где порт указан в ```pom.xml``` в разделе настройки плагина ```tomcat7-maven-plugin```