CREATE TABLE public.users
(
  id SERIAL PRIMARY KEY,
  username VARCHAR(255) NOT NULL,
  password_hash VARCHAR(60) NOT NULL,
  created_dt TIMESTAMP NOT NULL
);
CREATE UNIQUE INDEX users__username__index ON public.users (username);
COMMENT ON COLUMN public.users.password_hash IS 'Хэш пароля';
COMMENT ON COLUMN public.users.created_dt IS 'Дата и время создания пользователя';
COMMENT ON TABLE public.users IS 'Пользователи';