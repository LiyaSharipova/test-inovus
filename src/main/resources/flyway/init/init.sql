CREATE DATABASE test_inovus;
CREATE USER test_inovus_user WITH password 'test_inovus_password';
GRANT ALL ON DATABASE test_inovus TO test_inovus_user;