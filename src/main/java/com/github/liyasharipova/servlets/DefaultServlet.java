package com.github.liyasharipova.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by liya on 17.10.17.
 */
public class DefaultServlet extends HttpServlet {

    final static Logger LOGGER = LoggerFactory.getLogger(DefaultServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Перенаправление с главной страницы на /welcome");
        resp.sendRedirect("/welcome");
    }
}
