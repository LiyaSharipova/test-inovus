package com.github.liyasharipova.servlets;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Запуск миграций при старте приложения
 */
public class MigrationStarter implements ServletContextListener {
    final static Logger LOGGER = LoggerFactory.getLogger(MigrationStarter.class);

    /**
     * метод вызывается после того, как конфигурация проекта настроена
     * и приложение запущено
     *
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Flyway flyway = new Flyway();

        // Point it to the database
        String url = System.getProperty("DB_URL");
        String user = System.getProperty("DB_USER");
        String password = System.getProperty("DB_PASSWORD");
        flyway.setDataSource(url, user, password);
        flyway.setLocations("flyway/migrations");

        // Start the migration
        int successfullMigrations = flyway.migrate();

        LOGGER.info("Миграции применились в количестве {} штук", successfullMigrations);

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}