package com.github.liyasharipova.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by liya on 15.10.17.
 */
public class WelcomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if (req.getSession().getAttribute("user") == null) {
            resp.sendRedirect("/sign-in");
        } else {
            String message = "";
            Calendar calendar = Calendar.getInstance();
            int hours = calendar.get(Calendar.HOUR_OF_DAY);
            if (hours >= 6 && hours < 10) {
                message = "Доброе утро";
            } else if (hours >= 10 && hours < 18) {
                message = "Добрый день";
            } else if (hours >= 18 && hours < 22) {
                message = "Добрый вечер";
            } else {
                message = "Доброй ночи";
            }
            req.setAttribute("message", message);
            req.getRequestDispatcher("welcome.jsp").forward(req, resp);
        }

    }
}
