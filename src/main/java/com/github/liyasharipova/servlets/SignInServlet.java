package com.github.liyasharipova.servlets;

import com.github.liyasharipova.models.User;
import com.github.liyasharipova.services.UserServiceInterface;
import com.github.liyasharipova.services.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by liya on 15.10.17.
 */

public class SignInServlet extends HttpServlet {

    private final static Logger LOGGER = LoggerFactory.getLogger(SignInServlet.class);

    private UserServiceInterface userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("user") == null) {
            req.getRequestDispatcher("signIn.jsp").forward(req, resp);
        } else resp.sendRedirect("/welcome");
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        userService = new UserServiceImpl();
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            String warning = "Необходимо ввести учетные данные";
            req.setAttribute("warning", warning);
            LOGGER.warn(warning);
            req.getRequestDispatcher("signIn.jsp").forward(req, resp);
        }
        User user = userService.find(username.trim(), password);
        if (user == null) {
            String error = "Имя пользователя и пароль не подходят";
            req.setAttribute("error", error);
            LOGGER.warn(error + ": " + username);
            req.getRequestDispatcher("signIn.jsp").forward(req, resp);
        } else {
            req.getSession().setAttribute("user", user);
            Cookie usernameCookie = new Cookie("username", username);
            usernameCookie.setMaxAge(60 * 60 * 24);
            resp.addCookie(usernameCookie);
            LOGGER.info("Пользователь вошел в систему: " + username);
            resp.sendRedirect("/welcome");
        }
    }
}
