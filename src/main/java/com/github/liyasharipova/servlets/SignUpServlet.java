package com.github.liyasharipova.servlets;

import com.github.liyasharipova.helpers.UserHelper;
import com.github.liyasharipova.models.User;
import com.github.liyasharipova.services.UserServiceInterface;
import com.github.liyasharipova.services.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by liya on 16.10.17.
 */
public class SignUpServlet extends HttpServlet {
    private final static Logger LOGGER = LoggerFactory.getLogger(SignUpServlet.class);

    private UserServiceInterface userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("user") == null) {
            req.getRequestDispatcher("signUp.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("/welcome");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        userService = new UserServiceImpl();
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String rePassword = req.getParameter("re_password");

        if (username == null || username.isEmpty()
                || password == null || password.isEmpty()
                || rePassword == null || rePassword.isEmpty()) {
            req.setAttribute("warning", "Необходимо ввести учетные данные");
        }

        List<String> errors = UserHelper.check(username, password, rePassword);
        if (userService.exists(username)) {
            errors.add("Пользователь с таким именем уже зарегистрирован");
        }

        if (errors.isEmpty()) {
            User user = userService.addUser(username, password);
            req.getSession().setAttribute("user", user);
            Cookie usernameCookie = new Cookie("username", username);
            usernameCookie.setMaxAge(60 * 60 * 24);
            resp.addCookie(usernameCookie);
            LOGGER.info("Успешно зарегистрирован пользователь: {}", username);
            resp.sendRedirect("/welcome");
        } else {
            req.setAttribute("errors", errors);
            LOGGER.warn("Нашлись ошибки при проверке данных пользователя: {}", Arrays.toString(errors.toArray()));
            req.getRequestDispatcher("signUp.jsp").forward(req, resp);
        }

    }
}
