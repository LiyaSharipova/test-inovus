package com.github.liyasharipova.repositories.Impl;

import com.github.liyasharipova.models.User;
import com.github.liyasharipova.repositories.UserRepoInterface;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by liya on 19.10.17.
 */
public class UserRepoImpl implements UserRepoInterface {
    private DataSource dataSource;

    public UserRepoImpl() {
        try {
            // Get DataSource
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            dataSource = (DataSource) envContext.lookup("jdbc/test_inovus");

        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addUser(User user) {
        try (Connection connection = dataSource.getConnection()) {

            try (PreparedStatement ps = connection.prepareStatement("INSERT INTO public.users " +
                    "(username, password_hash, created_dt) VALUES (?,?,?)")) {
                ps.setString(1, user.getUsername());
                ps.setString(2, user.getPassword());
                ps.setTimestamp(3, user.getCreated_dt());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public User getUserByUsername(String username) {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM public.users" +
                    " WHERE (username = ?)")) {

                ps.setString(1, username);
                try (ResultSet rs = ps.executeQuery()) {

                    while (rs.next()) {
                        return new User(rs.getString("username"),
                                rs.getString("password_hash"),
                                rs.getTimestamp("created_dt"));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
