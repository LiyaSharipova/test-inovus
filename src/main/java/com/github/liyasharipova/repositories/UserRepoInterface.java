package com.github.liyasharipova.repositories;

import com.github.liyasharipova.models.User;

/**
 * Created by liya on 19.10.17.
 */
public interface UserRepoInterface {
    void addUser(User user);

    User getUserByUsername(String username);

}
