package com.github.liyasharipova.helpers;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by liya on 18.10.17.
 */
public class UserHelper {


    public static List<String> check(String username, String password,
                                     String rePasword) {
        List<String> errors = new LinkedList<String>();
        if (!usernameCheck(username)) {
            errors.add("Имя пользователя должно быть длиннее " +
                    "4 символов и состоять из цифр и букв" +
                    " английского алфавита");
        }
        if (!passwordEquals(password, rePasword)) {
            errors.add("Пароль и повтор пароля не совпадают");
        }
        if(!passwordCheck(password)){
            errors.add("Пароль недостаточно сложен: должны быть цифры," +
                    " заглавные и строчные буквы и длина минимум 8 символов");
        }

        return errors;
    }

    public static boolean usernameCheck(String username) {
        Pattern p = Pattern.compile("[A-Za-z0-9]{4,}");
        Matcher m = p.matcher(username);
        return m.matches();
    }

    public static boolean passwordEquals(String password, String rePassword) {
        return password.equals(rePassword);
    }
    public static boolean passwordCheck(String password){
        Pattern p = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+${8,}");
        Matcher m = p.matcher(password);
        return m.matches();
    }


}
