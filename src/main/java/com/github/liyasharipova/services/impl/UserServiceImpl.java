package com.github.liyasharipova.services.impl;

import com.github.liyasharipova.models.User;
import com.github.liyasharipova.repositories.Impl.UserRepoImpl;
import com.github.liyasharipova.repositories.UserRepoInterface;
import com.github.liyasharipova.services.UserServiceInterface;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.Timestamp;

/**
 * Created by liya on 19.10.17.
 */
public class UserServiceImpl implements UserServiceInterface {
    private UserRepoInterface userRepo;

    public UserServiceImpl() {
        this.userRepo = new UserRepoImpl();
    }

    @Override
    public User find(String username, String password) {
        User user = userRepo.getUserByUsername(username);
        if (user != null) {
            if (BCrypt.checkpw(password, user.getPassword())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User addUser(String username, String password) {
        userRepo = new UserRepoImpl();
        String passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
        Timestamp created_dt = new Timestamp(System.currentTimeMillis());
        User user = new User(username, passwordHash, created_dt);
        userRepo.addUser(user);
        return user;
    }

    @Override
    public boolean exists(String username) {
        return userRepo.getUserByUsername(username) != null;
    }
}
