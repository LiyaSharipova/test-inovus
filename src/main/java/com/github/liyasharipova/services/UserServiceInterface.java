package com.github.liyasharipova.services;

import com.github.liyasharipova.models.User;

/**
 * Created by liya on 19.10.17.
 */
public interface UserServiceInterface {
    User find(String username, String password);

    User addUser(String username, String password);

    boolean exists(String username);
}
