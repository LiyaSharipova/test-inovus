package com.github.liyasharipova.models;

import java.sql.Timestamp;

/**
 * Created by liya on 18.10.17.
 */
public class User {
    private String username;
    private String password;
    private Timestamp created_dt;

    public User(String username, String password, Timestamp created_dt) {
        this.username = username;
        this.password = password;
        this.created_dt = created_dt;
    }

//    public User(String username, String password) {
//        this.username = username;
//        this.password = password;
//        created_dt = new Timestamp(System.currentTimeMillis());
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreated_dt() {
        return created_dt;
    }

    public void setCreated_dt(Timestamp created_dt) {
        this.created_dt = created_dt;
    }
}
