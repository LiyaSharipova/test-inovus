<%--
  Created by IntelliJ IDEA.
  User: liya
  Date: 16.10.17
  Time: 0:06
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>Sign Up</title>
</head>
<body>
<p style="color: orange">
    ${requestScope.warning}
</p>
<c:forEach items="${requestScope.errors}" var="error">
    <p style="color: red">
        <c:out value="${error}"/>
    </p>
</c:forEach>


<form name="signUp" method="post" action="/sign-up">
    <table>
        <tr>
            <td>
                <label for="username">Имя пользователя</label>
            </td>
            <td>
                <input type="text" name="username" value="${username}" id="username">
            </td>
        </tr>
        <tr>
            <td>
                <label for="password">Пароль</label>
            </td>
            <td>
                <input type="password" name="password" id="password">
            </td>
        </tr>
        <tr>
            <td>
                <label for="re_password">Повтор пароля</label>
            </td>
            <td>
                <input type="password" name="re_password" id="re_password">
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Зарегистрироваться">
            </td>
        </tr>
    </table>
    <%--<p>--%>
        <%--<label for="username">Имя пользователя</label>--%>
        <%--<input type="text" name="username" id="username">--%>
    <%--</p>--%>
    <%--<p>--%>
        <%--<label for="password">Пароль</label>--%>
        <%--<input type="text" name="password" id="password">--%>
    <%--</p>--%>
    <%--<p>--%>
        <%--<label for="re_password">Пароль</label>--%>
        <%--<input type="text" name="re_password" id="re_password">--%>
    <%--</p>--%>
    <%--<input type="submit" value="Зарегистрироваться">--%>
</form>
</body>
</html>
