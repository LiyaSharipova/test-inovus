<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: liya
  Date: 15.10.17
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>Sign In</title>
</head>
<body>
<p style="color: green">
    <c:out value="${sessionScope.message}"/>
</p>
<p style="color: orange">
    <c:out value="${requestScope.warning}"/>
</p>
<p style="color: red">
    <c:out value="${requestScope.error}"/>
</p>
<c:remove var="message" scope="session"/>
<form name="signIn" method="post">
    <table>
        <tr>
            <td>
                <label for="username">Имя пользователя</label>
            </td>
            <td>
                <input type="text" name="username" value="${cookie.username.value}" id="username">
            </td>
            <td>
                <a href="/sign-up">Регистрация</a>
            </td>
        </tr>
        <tr>
            <td>
                <label for="password">Пароль</label>
            </td>
            <td>
                <input type="password" name="password" id="password">
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Войти"></td>
        </tr>
    </table>
</form>
</body>
</html>
